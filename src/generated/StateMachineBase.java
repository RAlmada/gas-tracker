/**
 * This class contains generated code from the Codename One Designer, DO NOT MODIFY!
 * This class is designed for subclassing that way the code generator can overwrite it
 * anytime without erasing your changes which should exist in a subclass!
 * For details about this file and how it works please read this blog post:
 * http://codenameone.blogspot.com/2010/10/ui-builder-class-how-to-actually-use.html
*/
package generated;

import com.codename1.ui.*;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;

import java.util.Hashtable;

public abstract class StateMachineBase extends UIBuilder {
    private Container aboutToShowThisContainer;
    /**
     * this method should be used to initialize variables instead of
     * the constructor/class scope to avoid race conditions
     */
    /**
    * @deprecated use the version that accepts a resource as an argument instead
    
**/
    protected void initVars() {}

    protected void initVars(Resources res) {}

    public StateMachineBase(Resources res, String resPath, boolean loadTheme) {
        startApp(res, resPath, loadTheme);
    }

    public Container startApp(Resources res, String resPath, boolean loadTheme) {
        initVars();
        UIBuilder.registerCustomComponent("Button", com.codename1.ui.Button.class);
        UIBuilder.registerCustomComponent("Tabs", com.codename1.ui.Tabs.class);
        UIBuilder.registerCustomComponent("MultiButton", com.codename1.components.MultiButton.class);
        UIBuilder.registerCustomComponent("Form", com.codename1.ui.Form.class);
        UIBuilder.registerCustomComponent("RadioButton", com.codename1.ui.RadioButton.class);
        UIBuilder.registerCustomComponent("Dialog", com.codename1.ui.Dialog.class);
        UIBuilder.registerCustomComponent("Label", com.codename1.ui.Label.class);
        UIBuilder.registerCustomComponent("SpanLabel", com.codename1.components.SpanLabel.class);
        UIBuilder.registerCustomComponent("TextField", com.codename1.ui.TextField.class);
        UIBuilder.registerCustomComponent("Container", com.codename1.ui.Container.class);
        if(loadTheme) {
            if(res == null) {
                try {
                    if(resPath.endsWith(".res")) {
                        res = Resources.open(resPath);
                        System.out.println("Warning: you should construct the state machine without the .res extension to allow theme overlays");
                    } else {
                        res = Resources.openLayered(resPath);
                    }
                } catch(java.io.IOException err) { err.printStackTrace(); }
            }
            initTheme(res);
        }
        if(res != null) {
            setResourceFilePath(resPath);
            setResourceFile(res);
            initVars(res);
            return showForm(getFirstFormName(), null);
        } else {
            Form f = (Form)createContainer(resPath, getFirstFormName());
            initVars(fetchResourceFile());
            beforeShow(f);
            f.show();
            postShow(f);
            return f;
        }
    }

    protected String getFirstFormName() {
        return "Tab";
    }

    public Container createWidget(Resources res, String resPath, boolean loadTheme) {
        initVars();
        UIBuilder.registerCustomComponent("Button", com.codename1.ui.Button.class);
        UIBuilder.registerCustomComponent("Tabs", com.codename1.ui.Tabs.class);
        UIBuilder.registerCustomComponent("MultiButton", com.codename1.components.MultiButton.class);
        UIBuilder.registerCustomComponent("Form", com.codename1.ui.Form.class);
        UIBuilder.registerCustomComponent("RadioButton", com.codename1.ui.RadioButton.class);
        UIBuilder.registerCustomComponent("Dialog", com.codename1.ui.Dialog.class);
        UIBuilder.registerCustomComponent("Label", com.codename1.ui.Label.class);
        UIBuilder.registerCustomComponent("SpanLabel", com.codename1.components.SpanLabel.class);
        UIBuilder.registerCustomComponent("TextField", com.codename1.ui.TextField.class);
        UIBuilder.registerCustomComponent("Container", com.codename1.ui.Container.class);
        if(loadTheme) {
            if(res == null) {
                try {
                    res = Resources.openLayered(resPath);
                } catch(java.io.IOException err) { err.printStackTrace(); }
            }
            initTheme(res);
        }
        return createContainer(resPath, "Tab");
    }

    protected void initTheme(Resources res) {
            String[] themes = res.getThemeResourceNames();
            if(themes != null && themes.length > 0) {
                UIManager.getInstance().setThemeProps(res.getTheme(themes[0]));
            }
    }

    public StateMachineBase() {
    }

    public StateMachineBase(String resPath) {
        this(null, resPath, true);
    }

    public StateMachineBase(Resources res) {
        this(res, null, true);
    }

    public StateMachineBase(String resPath, boolean loadTheme) {
        this(null, resPath, loadTheme);
    }

    public StateMachineBase(Resources res, boolean loadTheme) {
        this(res, null, loadTheme);
    }

    public com.codename1.ui.Container findContainer1(Component root) {
        return (com.codename1.ui.Container)findByName("Container1", root);
    }

    public com.codename1.ui.Container findContainer1() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container1", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container1", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findCarSubmit(Component root) {
        return (com.codename1.ui.Button)findByName("CarSubmit", root);
    }

    public com.codename1.ui.Button findCarSubmit() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("CarSubmit", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("CarSubmit", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findGallonsLabel(Component root) {
        return (com.codename1.ui.Label)findByName("GallonsLabel", root);
    }

    public com.codename1.ui.Label findGallonsLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("GallonsLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("GallonsLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.components.SpanLabel findCdLabel(Component root) {
        return (com.codename1.components.SpanLabel)findByName("cdLabel", root);
    }

    public com.codename1.components.SpanLabel findCdLabel() {
        com.codename1.components.SpanLabel cmp = (com.codename1.components.SpanLabel)findByName("cdLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.components.SpanLabel)findByName("cdLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.RadioButton findTripButton(Component root) {
        return (com.codename1.ui.RadioButton)findByName("TripButton", root);
    }

    public com.codename1.ui.RadioButton findTripButton() {
        com.codename1.ui.RadioButton cmp = (com.codename1.ui.RadioButton)findByName("TripButton", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.RadioButton)findByName("TripButton", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCPMLabel(Component root) {
        return (com.codename1.ui.Label)findByName("CPMLabel", root);
    }

    public com.codename1.ui.Label findCPMLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("CPMLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("CPMLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findGAdd(Component root) {
        return (com.codename1.ui.Label)findByName("GAdd", root);
    }

    public com.codename1.ui.Label findGAdd() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("GAdd", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("GAdd", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findChoiceButtonContainer(Component root) {
        return (com.codename1.ui.Container)findByName("ChoiceButtonContainer", root);
    }

    public com.codename1.ui.Container findChoiceButtonContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("ChoiceButtonContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("ChoiceButtonContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findBreakdownTab(Component root) {
        return (com.codename1.ui.Container)findByName("BreakdownTab", root);
    }

    public com.codename1.ui.Container findBreakdownTab() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("BreakdownTab", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("BreakdownTab", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findMTF(Component root) {
        return (com.codename1.ui.Label)findByName("MTF", root);
    }

    public com.codename1.ui.Label findMTF() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("MTF", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("MTF", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Tabs findMenu(Component root) {
        return (com.codename1.ui.Tabs)findByName("Menu", root);
    }

    public com.codename1.ui.Tabs findMenu() {
        com.codename1.ui.Tabs cmp = (com.codename1.ui.Tabs)findByName("Menu", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Tabs)findByName("Menu", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findWelcomeContainer(Component root) {
        return (com.codename1.ui.Container)findByName("WelcomeContainer", root);
    }

    public com.codename1.ui.Container findWelcomeContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("WelcomeContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("WelcomeContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findMTFLabel(Component root) {
        return (com.codename1.ui.Label)findByName("MTFLabel", root);
    }

    public com.codename1.ui.Label findMTFLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("MTFLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("MTFLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCompletefilllabel1(Component root) {
        return (com.codename1.ui.Label)findByName("completefilllabel1", root);
    }

    public com.codename1.ui.Label findCompletefilllabel1() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("completefilllabel1", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("completefilllabel1", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.RadioButton findOdometerButton(Component root) {
        return (com.codename1.ui.RadioButton)findByName("OdometerButton", root);
    }

    public com.codename1.ui.RadioButton findOdometerButton() {
        com.codename1.ui.RadioButton cmp = (com.codename1.ui.RadioButton)findByName("OdometerButton", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.RadioButton)findByName("OdometerButton", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findEditEntry(Component root) {
        return (com.codename1.ui.Container)findByName("EditEntry", root);
    }

    public com.codename1.ui.Container findEditEntry() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("EditEntry", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("EditEntry", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findGAddLabel(Component root) {
        return (com.codename1.ui.Label)findByName("GAddLabel", root);
    }

    public com.codename1.ui.Label findGAddLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("GAddLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("GAddLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findCostOfFillup(Component root) {
        return (com.codename1.ui.TextField)findByName("CostOfFillup", root);
    }

    public com.codename1.ui.TextField findCostOfFillup() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("CostOfFillup", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("CostOfFillup", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findGasTankSize(Component root) {
        return (com.codename1.ui.TextField)findByName("GasTankSize", root);
    }

    public com.codename1.ui.TextField findGasTankSize() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("GasTankSize", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("GasTankSize", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findDateLabel(Component root) {
        return (com.codename1.ui.Label)findByName("DateLabel", root);
    }

    public com.codename1.ui.Label findDateLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("DateLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("DateLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.RadioButton findCompletefillno(Component root) {
        return (com.codename1.ui.RadioButton)findByName("completefillno", root);
    }

    public com.codename1.ui.RadioButton findCompletefillno() {
        com.codename1.ui.RadioButton cmp = (com.codename1.ui.RadioButton)findByName("completefillno", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.RadioButton)findByName("completefillno", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findMilesSinceFillup(Component root) {
        return (com.codename1.ui.TextField)findByName("MilesSinceFillup", root);
    }

    public com.codename1.ui.TextField findMilesSinceFillup() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("MilesSinceFillup", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("MilesSinceFillup", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findDollaLabel(Component root) {
        return (com.codename1.ui.Label)findByName("DollaLabel", root);
    }

    public com.codename1.ui.Label findDollaLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("DollaLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("DollaLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findSOTLabel(Component root) {
        return (com.codename1.ui.Label)findByName("SOTLabel", root);
    }

    public com.codename1.ui.Label findSOTLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("SOTLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("SOTLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findCarName(Component root) {
        return (com.codename1.ui.TextField)findByName("CarName", root);
    }

    public com.codename1.ui.TextField findCarName() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("CarName", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("CarName", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCost(Component root) {
        return (com.codename1.ui.Label)findByName("Cost", root);
    }

    public com.codename1.ui.Label findCost() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Cost", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Cost", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findNewEntryTab(Component root) {
        return (com.codename1.ui.Container)findByName("NewEntryTab", root);
    }

    public com.codename1.ui.Container findNewEntryTab() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("NewEntryTab", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("NewEntryTab", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findCarContainer(Component root) {
        return (com.codename1.ui.Container)findByName("CarContainer", root);
    }

    public com.codename1.ui.Container findCarContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("CarContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("CarContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCurrentOdo(Component root) {
        return (com.codename1.ui.Label)findByName("CurrentOdo", root);
    }

    public com.codename1.ui.Label findCurrentOdo() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("CurrentOdo", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("CurrentOdo", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.components.MultiButton findMultiButton(Component root) {
        return (com.codename1.components.MultiButton)findByName("MultiButton", root);
    }

    public com.codename1.components.MultiButton findMultiButton() {
        com.codename1.components.MultiButton cmp = (com.codename1.components.MultiButton)findByName("MultiButton", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.components.MultiButton)findByName("MultiButton", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findDecisionLabel(Component root) {
        return (com.codename1.ui.Label)findByName("DecisionLabel", root);
    }

    public com.codename1.ui.Label findDecisionLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("DecisionLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("DecisionLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCostLabel(Component root) {
        return (com.codename1.ui.Label)findByName("CostLabel", root);
    }

    public com.codename1.ui.Label findCostLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("CostLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("CostLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findDBFLabel(Component root) {
        return (com.codename1.ui.Label)findByName("DBFLabel", root);
    }

    public com.codename1.ui.Label findDBFLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("DBFLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("DBFLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCPG(Component root) {
        return (com.codename1.ui.Label)findByName("CPG", root);
    }

    public com.codename1.ui.Label findCPG() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("CPG", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("CPG", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findPrevEntries(Component root) {
        return (com.codename1.ui.Container)findByName("prevEntries", root);
    }

    public com.codename1.ui.Container findPrevEntries() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("prevEntries", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("prevEntries", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findSubmit(Component root) {
        return (com.codename1.ui.Button)findByName("Submit", root);
    }

    public com.codename1.ui.Button findSubmit() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("Submit", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("Submit", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findNoButton(Component root) {
        return (com.codename1.ui.Button)findByName("noButton", root);
    }

    public com.codename1.ui.Button findNoButton() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("noButton", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("noButton", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findDetailsTitleLabel(Component root) {
        return (com.codename1.ui.Label)findByName("DetailsTitleLabel", root);
    }

    public com.codename1.ui.Label findDetailsTitleLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("DetailsTitleLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("DetailsTitleLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findGasTankSizeLabel(Component root) {
        return (com.codename1.ui.Label)findByName("GasTankSizeLabel", root);
    }

    public com.codename1.ui.Label findGasTankSizeLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("GasTankSizeLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("GasTankSizeLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findDetailsTitleContainer(Component root) {
        return (com.codename1.ui.Container)findByName("DetailsTitleContainer", root);
    }

    public com.codename1.ui.Container findDetailsTitleContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("DetailsTitleContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("DetailsTitleContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCPM(Component root) {
        return (com.codename1.ui.Label)findByName("CPM", root);
    }

    public com.codename1.ui.Label findCPM() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("CPM", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("CPM", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.components.SpanLabel findChoiceLabel(Component root) {
        return (com.codename1.components.SpanLabel)findByName("ChoiceLabel", root);
    }

    public com.codename1.components.SpanLabel findChoiceLabel() {
        com.codename1.components.SpanLabel cmp = (com.codename1.components.SpanLabel)findByName("ChoiceLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.components.SpanLabel)findByName("ChoiceLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findStatsTab(Component root) {
        return (com.codename1.ui.Container)findByName("StatsTab", root);
    }

    public com.codename1.ui.Container findStatsTab() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("StatsTab", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("StatsTab", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCarNameLabel(Component root) {
        return (com.codename1.ui.Label)findByName("CarNameLabel", root);
    }

    public com.codename1.ui.Label findCarNameLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("CarNameLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("CarNameLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCurrentCarLabel(Component root) {
        return (com.codename1.ui.Label)findByName("CurrentCarLabel", root);
    }

    public com.codename1.ui.Label findCurrentCarLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("CurrentCarLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("CurrentCarLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findCurrentMilage(Component root) {
        return (com.codename1.ui.TextField)findByName("CurrentMilage", root);
    }

    public com.codename1.ui.TextField findCurrentMilage() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("CurrentMilage", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("CurrentMilage", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findCompletefillcontainer(Component root) {
        return (com.codename1.ui.Container)findByName("completefillcontainer", root);
    }

    public com.codename1.ui.Container findCompletefillcontainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("completefillcontainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("completefillcontainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCurrentMilageLabel(Component root) {
        return (com.codename1.ui.Label)findByName("CurrentMilageLabel", root);
    }

    public com.codename1.ui.Label findCurrentMilageLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("CurrentMilageLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("CurrentMilageLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findPastEntriesTab(Component root) {
        return (com.codename1.ui.Container)findByName("PastEntriesTab", root);
    }

    public com.codename1.ui.Container findPastEntriesTab() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("PastEntriesTab", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("PastEntriesTab", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.RadioButton findCompletefillyes(Component root) {
        return (com.codename1.ui.RadioButton)findByName("completefillyes", root);
    }

    public com.codename1.ui.RadioButton findCompletefillyes() {
        com.codename1.ui.RadioButton cmp = (com.codename1.ui.RadioButton)findByName("completefillyes", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.RadioButton)findByName("completefillyes", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findButtonContainer(Component root) {
        return (com.codename1.ui.Container)findByName("ButtonContainer", root);
    }

    public com.codename1.ui.Container findButtonContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("ButtonContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("ButtonContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findDateField(Component root) {
        return (com.codename1.ui.TextField)findByName("DateField", root);
    }

    public com.codename1.ui.TextField findDateField() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("DateField", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("DateField", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findDeleteButton(Component root) {
        return (com.codename1.ui.Button)findByName("DeleteButton", root);
    }

    public com.codename1.ui.Button findDeleteButton() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("DeleteButton", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("DeleteButton", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findCancel(Component root) {
        return (com.codename1.ui.Button)findByName("Cancel", root);
    }

    public com.codename1.ui.Button findCancel() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("Cancel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("Cancel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findDone(Component root) {
        return (com.codename1.ui.Button)findByName("Done", root);
    }

    public com.codename1.ui.Button findDone() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("Done", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("Done", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findDeleteAll(Component root) {
        return (com.codename1.ui.Button)findByName("DeleteAll", root);
    }

    public com.codename1.ui.Button findDeleteAll() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("DeleteAll", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("DeleteAll", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findMPGLabel(Component root) {
        return (com.codename1.ui.Label)findByName("MPGLabel", root);
    }

    public com.codename1.ui.Label findMPGLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("MPGLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("MPGLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findContainer(Component root) {
        return (com.codename1.ui.Container)findByName("Container", root);
    }

    public com.codename1.ui.Container findContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("Container", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("Container", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.TextField findGallonsAdded(Component root) {
        return (com.codename1.ui.TextField)findByName("GallonsAdded", root);
    }

    public com.codename1.ui.TextField findGallonsAdded() {
        com.codename1.ui.TextField cmp = (com.codename1.ui.TextField)findByName("GallonsAdded", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.TextField)findByName("GallonsAdded", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findCompletefillradiocontainer(Component root) {
        return (com.codename1.ui.Container)findByName("completefillradiocontainer", root);
    }

    public com.codename1.ui.Container findCompletefillradiocontainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("completefillradiocontainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("completefillradiocontainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findEditButton(Component root) {
        return (com.codename1.ui.Button)findByName("EditButton", root);
    }

    public com.codename1.ui.Button findEditButton() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("EditButton", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("EditButton", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findCarCancel(Component root) {
        return (com.codename1.ui.Button)findByName("CarCancel", root);
    }

    public com.codename1.ui.Button findCarCancel() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("CarCancel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("CarCancel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCurrentOdoLabel(Component root) {
        return (com.codename1.ui.Label)findByName("CurrentOdoLabel", root);
    }

    public com.codename1.ui.Label findCurrentOdoLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("CurrentOdoLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("CurrentOdoLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findDBF(Component root) {
        return (com.codename1.ui.Label)findByName("DBF", root);
    }

    public com.codename1.ui.Label findDBF() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("DBF", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("DBF", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Button findYesButton(Component root) {
        return (com.codename1.ui.Button)findByName("yesButton", root);
    }

    public com.codename1.ui.Button findYesButton() {
        com.codename1.ui.Button cmp = (com.codename1.ui.Button)findByName("yesButton", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Button)findByName("yesButton", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findMPG(Component root) {
        return (com.codename1.ui.Label)findByName("MPG", root);
    }

    public com.codename1.ui.Label findMPG() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("MPG", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("MPG", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findWelcomeLabel(Component root) {
        return (com.codename1.ui.Label)findByName("WelcomeLabel", root);
    }

    public com.codename1.ui.Label findWelcomeLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("WelcomeLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("WelcomeLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findCostContainer(Component root) {
        return (com.codename1.ui.Container)findByName("CostContainer", root);
    }

    public com.codename1.ui.Container findCostContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("CostContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("CostContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.components.MultiButton findMultiButton2(Component root) {
        return (com.codename1.components.MultiButton)findByName("MultiButton2", root);
    }

    public com.codename1.components.MultiButton findMultiButton2() {
        com.codename1.components.MultiButton cmp = (com.codename1.components.MultiButton)findByName("MultiButton2", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.components.MultiButton)findByName("MultiButton2", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.components.MultiButton findMultiButton1(Component root) {
        return (com.codename1.components.MultiButton)findByName("MultiButton1", root);
    }

    public com.codename1.components.MultiButton findMultiButton1() {
        com.codename1.components.MultiButton cmp = (com.codename1.components.MultiButton)findByName("MultiButton1", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.components.MultiButton)findByName("MultiButton1", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findSOT(Component root) {
        return (com.codename1.ui.Label)findByName("SOT", root);
    }

    public com.codename1.ui.Label findSOT() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("SOT", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("SOT", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findLabel(Component root) {
        return (com.codename1.ui.Label)findByName("Label", root);
    }

    public com.codename1.ui.Label findLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("Label", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("Label", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Label findCPGLabel(Component root) {
        return (com.codename1.ui.Label)findByName("CPGLabel", root);
    }

    public com.codename1.ui.Label findCPGLabel() {
        com.codename1.ui.Label cmp = (com.codename1.ui.Label)findByName("CPGLabel", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Label)findByName("CPGLabel", aboutToShowThisContainer);
        }
        return cmp;
    }

    public com.codename1.ui.Container findInfoContainer(Component root) {
        return (com.codename1.ui.Container)findByName("InfoContainer", root);
    }

    public com.codename1.ui.Container findInfoContainer() {
        com.codename1.ui.Container cmp = (com.codename1.ui.Container)findByName("InfoContainer", Display.getInstance().getCurrent());
        if(cmp == null && aboutToShowThisContainer != null) {
            cmp = (com.codename1.ui.Container)findByName("InfoContainer", aboutToShowThisContainer);
        }
        return cmp;
    }

    public static final int COMMAND_AddCarCancel = 1;

    protected boolean onAddCarCancel() {
        return false;
    }

    protected void processCommand(ActionEvent ev, Command cmd) {
        switch(cmd.getId()) {
            case COMMAND_AddCarCancel:
                if(onAddCarCancel()) {
                    ev.consume();
                    return;
                }
                break;

        }
        if(ev.getComponent() != null) {
            handleComponentAction(ev.getComponent(), ev);
        }
    }

    protected void exitForm(Form f) {
        if("Details".equals(f.getName())) {
            exitDetails(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("ConfDialog".equals(f.getName())) {
            exitConfDialog(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("addCar".equals(f.getName())) {
            exitAddCar(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Tab".equals(f.getName())) {
            exitTab(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("editData".equals(f.getName())) {
            exitEditData(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Settings".equals(f.getName())) {
            exitSettings(f);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void exitDetails(Form f) {
    }


    protected void exitConfDialog(Form f) {
    }


    protected void exitAddCar(Form f) {
    }


    protected void exitTab(Form f) {
    }


    protected void exitEditData(Form f) {
    }


    protected void exitSettings(Form f) {
    }

    protected void beforeShow(Form f) {
    aboutToShowThisContainer = f;
        if("Details".equals(f.getName())) {
            beforeDetails(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("ConfDialog".equals(f.getName())) {
            beforeConfDialog(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("addCar".equals(f.getName())) {
            beforeAddCar(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Tab".equals(f.getName())) {
            beforeTab(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("editData".equals(f.getName())) {
            beforeEditData(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Settings".equals(f.getName())) {
            beforeSettings(f);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void beforeDetails(Form f) {
    }


    protected void beforeConfDialog(Form f) {
    }


    protected void beforeAddCar(Form f) {
    }


    protected void beforeTab(Form f) {
    }


    protected void beforeEditData(Form f) {
    }


    protected void beforeSettings(Form f) {
    }

    protected void beforeShowContainer(Container c) {
        aboutToShowThisContainer = c;
        if("Details".equals(c.getName())) {
            beforeContainerDetails(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("ConfDialog".equals(c.getName())) {
            beforeContainerConfDialog(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("addCar".equals(c.getName())) {
            beforeContainerAddCar(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Tab".equals(c.getName())) {
            beforeContainerTab(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("editData".equals(c.getName())) {
            beforeContainerEditData(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Settings".equals(c.getName())) {
            beforeContainerSettings(c);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void beforeContainerDetails(Container c) {
    }


    protected void beforeContainerConfDialog(Container c) {
    }


    protected void beforeContainerAddCar(Container c) {
    }


    protected void beforeContainerTab(Container c) {
    }


    protected void beforeContainerEditData(Container c) {
    }


    protected void beforeContainerSettings(Container c) {
    }

    protected void postShow(Form f) {
        if("Details".equals(f.getName())) {
            postDetails(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("ConfDialog".equals(f.getName())) {
            postConfDialog(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("addCar".equals(f.getName())) {
            postAddCar(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Tab".equals(f.getName())) {
            postTab(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("editData".equals(f.getName())) {
            postEditData(f);
            aboutToShowThisContainer = null;
            return;
        }

        if("Settings".equals(f.getName())) {
            postSettings(f);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void postDetails(Form f) {
    }


    protected void postConfDialog(Form f) {
    }


    protected void postAddCar(Form f) {
    }


    protected void postTab(Form f) {
    }


    protected void postEditData(Form f) {
    }


    protected void postSettings(Form f) {
    }

    protected void postShowContainer(Container c) {
        if("Details".equals(c.getName())) {
            postContainerDetails(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("ConfDialog".equals(c.getName())) {
            postContainerConfDialog(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("addCar".equals(c.getName())) {
            postContainerAddCar(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Tab".equals(c.getName())) {
            postContainerTab(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("editData".equals(c.getName())) {
            postContainerEditData(c);
            aboutToShowThisContainer = null;
            return;
        }

        if("Settings".equals(c.getName())) {
            postContainerSettings(c);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void postContainerDetails(Container c) {
    }


    protected void postContainerConfDialog(Container c) {
    }


    protected void postContainerAddCar(Container c) {
    }


    protected void postContainerTab(Container c) {
    }


    protected void postContainerEditData(Container c) {
    }


    protected void postContainerSettings(Container c) {
    }

    protected void onCreateRoot(String rootName) {
        if("Details".equals(rootName)) {
            onCreateDetails();
            aboutToShowThisContainer = null;
            return;
        }

        if("ConfDialog".equals(rootName)) {
            onCreateConfDialog();
            aboutToShowThisContainer = null;
            return;
        }

        if("addCar".equals(rootName)) {
            onCreateAddCar();
            aboutToShowThisContainer = null;
            return;
        }

        if("Tab".equals(rootName)) {
            onCreateTab();
            aboutToShowThisContainer = null;
            return;
        }

        if("editData".equals(rootName)) {
            onCreateEditData();
            aboutToShowThisContainer = null;
            return;
        }

        if("Settings".equals(rootName)) {
            onCreateSettings();
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void onCreateDetails() {
    }


    protected void onCreateConfDialog() {
    }


    protected void onCreateAddCar() {
    }


    protected void onCreateTab() {
    }


    protected void onCreateEditData() {
    }


    protected void onCreateSettings() {
    }

    protected Hashtable getFormState(Form f) {
        Hashtable h = super.getFormState(f);
        if("Details".equals(f.getName())) {
            getStateDetails(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("ConfDialog".equals(f.getName())) {
            getStateConfDialog(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("addCar".equals(f.getName())) {
            getStateAddCar(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("Tab".equals(f.getName())) {
            getStateTab(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("editData".equals(f.getName())) {
            getStateEditData(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

        if("Settings".equals(f.getName())) {
            getStateSettings(f, h);
            aboutToShowThisContainer = null;
            return h;
        }

            return h;
    }


    protected void getStateDetails(Form f, Hashtable h) {
    }


    protected void getStateConfDialog(Form f, Hashtable h) {
    }


    protected void getStateAddCar(Form f, Hashtable h) {
    }


    protected void getStateTab(Form f, Hashtable h) {
    }


    protected void getStateEditData(Form f, Hashtable h) {
    }


    protected void getStateSettings(Form f, Hashtable h) {
    }

    protected void setFormState(Form f, Hashtable state) {
        super.setFormState(f, state);
        if("Details".equals(f.getName())) {
            setStateDetails(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("ConfDialog".equals(f.getName())) {
            setStateConfDialog(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("addCar".equals(f.getName())) {
            setStateAddCar(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("Tab".equals(f.getName())) {
            setStateTab(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("editData".equals(f.getName())) {
            setStateEditData(f, state);
            aboutToShowThisContainer = null;
            return;
        }

        if("Settings".equals(f.getName())) {
            setStateSettings(f, state);
            aboutToShowThisContainer = null;
            return;
        }

            return;
    }


    protected void setStateDetails(Form f, Hashtable state) {
    }


    protected void setStateConfDialog(Form f, Hashtable state) {
    }


    protected void setStateAddCar(Form f, Hashtable state) {
    }


    protected void setStateTab(Form f, Hashtable state) {
    }


    protected void setStateEditData(Form f, Hashtable state) {
    }


    protected void setStateSettings(Form f, Hashtable state) {
    }

    protected void handleComponentAction(Component c, ActionEvent event) {
        Container rootContainerAncestor = getRootAncestor(c);
        if(rootContainerAncestor == null) return;
        String rootContainerName = rootContainerAncestor.getName();
        Container leadParentContainer = c.getParent().getLeadParent();
        if(leadParentContainer != null && leadParentContainer.getClass() != Container.class) {
            c = c.getParent().getLeadParent();
        }
        if(rootContainerName == null) return;
        if(rootContainerName.equals("Details")) {
            if("EditButton".equals(c.getName())) {
                onDetails_EditButtonAction(c, event);
                return;
            }
            if("DeleteButton".equals(c.getName())) {
                onDetails_DeleteButtonAction(c, event);
                return;
            }
        }
        if(rootContainerName.equals("ConfDialog")) {
            if("yesButton".equals(c.getName())) {
                onConfDialog_YesButtonAction(c, event);
                return;
            }
            if("noButton".equals(c.getName())) {
                onConfDialog_NoButtonAction(c, event);
                return;
            }
        }
        if(rootContainerName.equals("addCar")) {
            if("CarName".equals(c.getName())) {
                onAddCar_CarNameAction(c, event);
                return;
            }
            if("CurrentMilage".equals(c.getName())) {
                onAddCar_CurrentMilageAction(c, event);
                return;
            }
            if("GasTankSize".equals(c.getName())) {
                onAddCar_GasTankSizeAction(c, event);
                return;
            }
            if("OdometerButton".equals(c.getName())) {
                onAddCar_OdometerButtonAction(c, event);
                return;
            }
            if("TripButton".equals(c.getName())) {
                onAddCar_TripButtonAction(c, event);
                return;
            }
            if("CarSubmit".equals(c.getName())) {
                onAddCar_CarSubmitAction(c, event);
                return;
            }
            if("CarCancel".equals(c.getName())) {
                onAddCar_CarCancelAction(c, event);
                return;
            }
        }
        if(rootContainerName.equals("Tab")) {
            if("DateField".equals(c.getName())) {
                onTab_DateFieldAction(c, event);
                return;
            }
            if("GallonsAdded".equals(c.getName())) {
                onTab_GallonsAddedAction(c, event);
                return;
            }
            if("MilesSinceFillup".equals(c.getName())) {
                onTab_MilesSinceFillupAction(c, event);
                return;
            }
            if("CostOfFillup".equals(c.getName())) {
                onTab_CostOfFillupAction(c, event);
                return;
            }
            if("completefillyes".equals(c.getName())) {
                onTab_CompletefillyesAction(c, event);
                return;
            }
            if("completefillno".equals(c.getName())) {
                onTab_CompletefillnoAction(c, event);
                return;
            }
            if("Submit".equals(c.getName())) {
                onTab_SubmitAction(c, event);
                return;
            }
            if("DeleteAll".equals(c.getName())) {
                onTab_DeleteAllAction(c, event);
                return;
            }
        }
        if(rootContainerName.equals("editData")) {
            if("DateField".equals(c.getName())) {
                onEditData_DateFieldAction(c, event);
                return;
            }
            if("GallonsAdded".equals(c.getName())) {
                onEditData_GallonsAddedAction(c, event);
                return;
            }
            if("MilesSinceFillup".equals(c.getName())) {
                onEditData_MilesSinceFillupAction(c, event);
                return;
            }
            if("CostOfFillup".equals(c.getName())) {
                onEditData_CostOfFillupAction(c, event);
                return;
            }
            if("completefillyes".equals(c.getName())) {
                onEditData_CompletefillyesAction(c, event);
                return;
            }
            if("completefillno".equals(c.getName())) {
                onEditData_CompletefillnoAction(c, event);
                return;
            }
            if("Done".equals(c.getName())) {
                onEditData_DoneAction(c, event);
                return;
            }
            if("Cancel".equals(c.getName())) {
                onEditData_CancelAction(c, event);
                return;
            }
        }
        if(rootContainerName.equals("Settings")) {
            if("MultiButton".equals(c.getName())) {
                onSettings_MultiButtonAction(c, event);
                return;
            }
            if("MultiButton1".equals(c.getName())) {
                onSettings_MultiButton1Action(c, event);
                return;
            }
            if("MultiButton2".equals(c.getName())) {
                onSettings_MultiButton2Action(c, event);
                return;
            }
        }
    }

      protected void onDetails_EditButtonAction(Component c, ActionEvent event) {
      }

      protected void onDetails_DeleteButtonAction(Component c, ActionEvent event) {
      }

      protected void onConfDialog_YesButtonAction(Component c, ActionEvent event) {
      }

      protected void onConfDialog_NoButtonAction(Component c, ActionEvent event) {
      }

      protected void onAddCar_CarNameAction(Component c, ActionEvent event) {
      }

      protected void onAddCar_CurrentMilageAction(Component c, ActionEvent event) {
      }

      protected void onAddCar_GasTankSizeAction(Component c, ActionEvent event) {
      }

      protected void onAddCar_OdometerButtonAction(Component c, ActionEvent event) {
      }

      protected void onAddCar_TripButtonAction(Component c, ActionEvent event) {
      }

      protected void onAddCar_CarSubmitAction(Component c, ActionEvent event) {
      }

      protected void onAddCar_CarCancelAction(Component c, ActionEvent event) {
      }

      protected void onTab_DateFieldAction(Component c, ActionEvent event) {
      }

      protected void onTab_GallonsAddedAction(Component c, ActionEvent event) {
      }

      protected void onTab_MilesSinceFillupAction(Component c, ActionEvent event) {
      }

      protected void onTab_CostOfFillupAction(Component c, ActionEvent event) {
      }

      protected void onTab_CompletefillyesAction(Component c, ActionEvent event) {
      }

      protected void onTab_CompletefillnoAction(Component c, ActionEvent event) {
      }

      protected void onTab_SubmitAction(Component c, ActionEvent event) {
      }

      protected void onTab_DeleteAllAction(Component c, ActionEvent event) {
      }

      protected void onEditData_DateFieldAction(Component c, ActionEvent event) {
      }

      protected void onEditData_GallonsAddedAction(Component c, ActionEvent event) {
      }

      protected void onEditData_MilesSinceFillupAction(Component c, ActionEvent event) {
      }

      protected void onEditData_CostOfFillupAction(Component c, ActionEvent event) {
      }

      protected void onEditData_CompletefillyesAction(Component c, ActionEvent event) {
      }

      protected void onEditData_CompletefillnoAction(Component c, ActionEvent event) {
      }

      protected void onEditData_DoneAction(Component c, ActionEvent event) {
      }

      protected void onEditData_CancelAction(Component c, ActionEvent event) {
      }

      protected void onSettings_MultiButtonAction(Component c, ActionEvent event) {
      }

      protected void onSettings_MultiButton1Action(Component c, ActionEvent event) {
      }

      protected void onSettings_MultiButton2Action(Component c, ActionEvent event) {
      }

}
