package userclasses;

import com.almadadev.gastracker.*;
import com.codename1.components.SpanButton;
import com.codename1.components.SpanLabel;
import com.codename1.io.Storage;
import com.codename1.io.Util;
import com.codename1.l10n.L10NManager;
import com.codename1.ui.*;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import generated.StateMachineBase;

import java.util.Vector;

/**
 * Created by Robert J. Almada on 5/31/14.
 * Edited by Robert J. Almada on 8/26/14.
 */
//TODO: Work on settings screen / class / functionality
@SuppressWarnings("unchecked")
public class StateMachine extends StateMachineBase {
    private Form root;
    private Form mainRoot;
    private String curCar = null;
    private int curNdx = -1;

    public StateMachine(String resFile) {
        super(resFile);
//        Storage.getInstance().deleteStorageFile("Durango");
//        Storage.getInstance().deleteStorageFile("name");
//        Storage.getInstance().writeObject("version", 0.5);
//        Storage.getInstance().deleteStorageFile("dataPoints");
        mainRoot = Display.getInstance().getCurrent();
        mainRoot.addOrientationListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                updatePrevData();
            }
        });
//        prevD[0] = findTanksMissed().getPreferredSize().getHeight();
//        prevD[1] = findTanksMissed().getPreferredSize().getWidth();
//        findTanksMissed().setPreferredSize(new Dimension(0, 0));
//        findTanksMissed().setVisible(false);
//        findNewEntryTab().revalidate();
//        prevD[2] = findForgetFillup().getPreferredSize().getHeight();
//        prevD[3] = findForgetFillup().getPreferredSize().getWidth();
//        findForgetFillup().setPreferredSize(new Dimension(0, 0));
//        findForgetFillup().setVisible(false);
        atRunTime();
        // do not modify, write code in initVars and initialize class members there,
        // the constructor might be invoked too late due to race conditions that might occur
    }

    /**
     * this method should be used to initialize variables instead of
     * the constructor/class scope to avoid race conditions
     */
    @SuppressWarnings("deprecation")
    protected void initVars(Resources res) {
        Util.register("DataPoint", DataPoint.class);
        Util.register("Car", Car.class);
        Util.register("NewCar", NewCar.class);
        Util.register("NewDataPoint", NewDataPoint.class);
        Util.register("NewDCar", NewDCar.class);
    }

//    @Override
//    protected void onTab_ForgetFillupAction(Component c, ActionEvent event) {
//        if (findForgetFillup(root).isSelected()) {
//            findTanksMissed(root).setVisible(true);
//            findTanksMissed(root).setPreferredSize(new Dimension(prevD[1],prevD[0]));
//        } else {
//            findTanksMissed(root).setVisible(false);
//            findTanksMissed(root).setPreferredSize(new Dimension(0,0));
//        }
//
//        findNewEntryTab(root).revalidate();
//    }

    @Override
    protected void onTab_SubmitAction(Component c, ActionEvent event) {
        String date;
        NewDCar car = (NewDCar)Storage.getInstance().readObject(curCar);
        NewDataPoint d;
        double GA, MSL, CTF;

        try {
            date = findDateField(root).getText();
            GA = Double.parseDouble(findGallonsAdded(root).getText());
            if (GA > car.getTankSize()) {
                onSubmitThrow("Error", "Cannot add more gas than size of tank: " + car.getTankSize());
                return;
            }
            MSL = Double.parseDouble(findMilesSinceFillup(root).getText());
            CTF = Double.parseDouble(findCostOfFillup(root).getText());
            if (car.getOdometer()) {
                if (MSL < car.getMileage()) {
                    onSubmitThrow("Error", "Odometer reading must be greater than current mileage stored!");
                    return;
                }
                d = new NewDataPoint(date,GA,MSL-car.getMileage(),CTF,MSL);
                car.setMileage(MSL);
            } else {
                car.setMileage(car.getMileage() + MSL);
                d = new NewDataPoint(date,GA,MSL,CTF,car.getMileage());
            }
            saveNewPoint(car, d);
            resetInputs();
            onSubmitThrow("Success", "Entry added successfully!");
            updatePrevData();
        } catch (NumberFormatException e) {
            onSubmitThrow("Error", "Gallons added, miles since fill/odometer reading, and cost must be numbers!");
        } catch (ArithmeticException e) {
            onSubmitThrow("Error", "Gallons added, miles since fill/odometer reading, and cost must be positive!");
        } catch (IllegalArgumentException e) {
            onSubmitThrow("Error", "Date must be filled in! (MM/DD/YY)");
        } catch (IndexOutOfBoundsException e) {
            onSubmitThrow("Error", "Illegal date! (MM/DD/YY)");
        }
    }

    @Override
    protected void onTab_DeleteAllAction(Component c, ActionEvent event) {
        final Dialog d = new Dialog();
        Style s = new Style();
        Button y = new Button("Yes");
        Button n = new Button("No");
        Container o = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container x = new Container(new FlowLayout(Component.CENTER));
        SpanLabel l = new SpanLabel("Are you sure you want to delete ALL previous entries?");
        Label t = new Label("Confirm Delete");

        n.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                d.dispose();
            }
        });
        y.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                NewDCar car = (NewDCar)Storage.getInstance().readObject(curCar);
                car.setMileage(car.getData().firstElement().getMileage() - car.getData().firstElement().getMSL());
                car.setData(new Vector<NewDataPoint>());
                Storage.getInstance().writeObject(curCar, car);
                updatePrevData();
                findCompletefillyes(root).setSelected(true);
                findCompletefillno(root).setEnabled(false);
                d.dispose();
            }
        });

        y.setUIID("GreenButton");
        n.setUIID("RedButton");

        y.setWidth(d.getPreferredW() / 3);
        n.setWidth(d.getPreferredW() / 3);

        t.setUIID("DialogTitle");
        l.setTextUIID("Label2");

        s.setBgColor(0);
        s.setFgColor(255);

        d.setDialogStyle(s);
        d.setTitleComponent(t);

        x.addComponent(y);
        x.addComponent(n);

        o.addComponent(l);
        o.addComponent(x);

        d.addComponent(o);
        final Command back = new Command("Back");
        y.getComponentForm().addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand() == back)
                    d.dispose();
            }
        });
        y.getComponentForm().setBackCommand(back);
        d.setDisposeWhenPointerOutOfBounds(true);
        d.show();
    }

    private void resetInputs() {
        findDateField(root).setText(null);
        findGallonsAdded(root).setText(null);
        findMilesSinceFillup(root).setText(null);
        findCostOfFillup(root).setText(null);
        findCompletefillyes(root).setSelected(true);
    }

    private void onSubmitThrow(String title, String body) {
        final Dialog d = new Dialog();
        Style s = new Style();
        Button b = new Button("OK");
        Container o = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        SpanLabel l = new SpanLabel(body);
        Label t = new Label(title);

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                d.dispose();
            }
        });
        b.setWidth(d.getPreferredW() / 2);

        s.setBgColor(0);
        s.setFgColor(255);

        t.setUIID("DialogTitle");
        l.setTextUIID("Label2");

        d.setDialogStyle(s);
        d.setTitleComponent(t);

        o.addComponent(l);
        o.addComponent(b);

        d.addComponent(o);
        final Command back = new Command("Back");
        b.getComponentForm().addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand() == back)
                    d.dispose();
            }
        });
        b.getComponentForm().setBackCommand(back);
        d.setDisposeWhenPointerOutOfBounds(true);
        d.show();
    }

    private void saveNewPoint(NewDCar c, NewDataPoint d) {
        Vector<NewDataPoint> a = c.getData();

        if (d.getCTF() < 0 || d.getGA() < 0 || d.getMSL() < 0)
            throw new ArithmeticException();

        if (findCompletefillno(root).isSelected()) {
            NewDataPoint prev = a.lastElement();
            prev.setGA(prev.getGA()+d.getGA());
            prev.setMSL(prev.getMSL()+d.getMSL());
            prev.setCTF(prev.getCTF()+d.getCTF());
            prev.setDate(d.getDate());
            prev.setMileage(d.getMileage());

            a.add(a.size()-1,prev);
        } else {
            a.add(d);
        }

        c.setData(a);
        Storage.getInstance().writeObject(curCar, c);
    }

    private void updatePrevData() {
        int ndx = 0;
        Container pd;
        NewDCar c = (NewDCar)Storage.getInstance().readObject(curCar);

        pd = findPrevEntries(root);

        if (pd != null)
            pd.removeAll();

        if (c != null) {
            if (c.getData().size() == 0) {
                findDeleteAll(root).setEnabled(false);
            } else {
                findDeleteAll(root).setEnabled(true);
                findDeleteAll(root).setUIID("RedButton");

                if (pd != null)
                    for (NewDataPoint d : c.getData())
                        pd.addComponent(makeEntry(d, ndx++));

                updateMileage(c);

                Storage.getInstance().writeObject(curCar, c);

                findPastEntriesTab(root).revalidate();
            }
        } else {
            findDeleteAll(root).setEnabled(false);
        }

        findCompletefillyes(root).setSelected(true);

        if (c.getData().isEmpty())
            findCompletefillno(root).setEnabled(false);
        else
            findCompletefillno(root).setEnabled(true);

        updateStats();
    }

    private void updateMileage(NewDCar c) {
        c.setMileage(c.getData().lastElement().getMileage());

        for (int i = c.getData().size()-2; i >= 0; i--)
            c.getData().get(i).setMileage(c.getData().get(i+1).getMileage() - c.getData().get(i+1).getMSL());

        Storage.getInstance().writeObject(curCar, c);
    }

    private Container makeEntry(final NewDataPoint d, final int ndx) {
        Container c = new Container();
        SpanButton s = new SpanButton("Date: " + d.getDate()
                + "\nGas Added: " + L10NManager.getInstance().format(d.getGA(),2)
                + "\nMiles Driven: " + L10NManager.getInstance().format(d.getMSL(),2)
                + "\nCost: " + L10NManager.getInstance().formatCurrency(d.getCTF()));

        s.setUIID("SpanLabel");

        s.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                curNdx = ndx;
                root = showForm("Details",null);
            }
        });

        c.addComponent(s);

        s.getUnselectedStyle().setBgTransparency(35);

        if (ndx % 2 == 0)
            s.getUnselectedStyle().setBgColor(0x333333);
        else
            s.getUnselectedStyle().setBgColor(0x666666);

        return c;
    }

    private void confirmDeleteDialog(String title, String body, final int ndx) {
        final Dialog d = new Dialog();
        Style s = new Style();
        Button y = new Button("Yes");
        Button n = new Button("No");
        Container o = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container x = new Container(new FlowLayout(Component.CENTER));
        SpanLabel l = new SpanLabel(body);
        Label t = new Label(title);

        d.setUIID("Form");
        d.setDialogUIID("Dialog");
        d.setDisposeWhenPointerOutOfBounds(true);

        n.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                d.dispose();
            }
        });
        y.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                root = mainRoot;
                NewDCar c = (NewDCar)Storage.getInstance().readObject(curCar);
                Vector<NewDataPoint> v = c.getData();
                v.remove(ndx);
                updatePrevData();
                c.setData(v);
                Storage.getInstance().writeObject(curCar, c);
                d.dispose();
                root.show();
            }
        });

        y.setUIID("GreenButton");
        n.setUIID("RedButton");

        y.setWidth(d.getPreferredW()/3);
        n.setWidth(d.getPreferredW()/3);

        t.setUIID("DialogTitle");
        l.setTextUIID("Label2");

        s.setBgColor(0);
        s.setFgColor(255);

        d.setDialogStyle(s);
        d.setTitleComponent(t);

        x.addComponent(y);
        x.addComponent(n);

        o.addComponent(l);
        o.addComponent(x);

        d.addComponent(o);

        final Command back = new Command("Back");
        y.getComponentForm().addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand() == back)
                    d.dispose();
            }
        });
        y.getComponentForm().setBackCommand(back);
        d.setDisposeWhenPointerOutOfBounds(true);
        d.show();
    }

    private void updateStats() {
        Label co = findCurrentOdo(root), mpg = findMPG(root), dbf = findDBF(root),
                cpg = findCPG(root), ga = findGAdd(root), cpm = findCPM(root),
                sot = findSOT(root);
        NewDCar c;
        Vector<NewDataPoint> v;

        if (Storage.getInstance().exists(curCar)) {
            c = (NewDCar)Storage.getInstance().readObject(curCar);
            v = c.getData();

            sot.setText(L10NManager.getInstance().format(c.getTankSize(),3) + " gallons");

            if (v.size() == 0) {
                co.setText(L10NManager.getInstance().format(c.getMileage(), 3) + " miles");
                mpg.setText("0 mpg");
                cpg.setText("$0.00");
                cpm.setText("$0.00");
                dbf.setText("0 miles");
                ga.setText("0 gallons");
            } else {
                co.setText(L10NManager.getInstance().format(c.getMileage(), 3) + " miles");
                mpg.setText(calcMPG(v));
                cpg.setText(calcCPG(v));
                cpm.setText(calcCPM(v));
                dbf.setText(calcDBF(v));
                ga.setText(calcGA(v));
            }
        } else {
            co.setText("0 miles");
            mpg.setText("0 mpg");
            cpg.setText("$0.00");
            cpm.setText("$0.00");
            dbf.setText("0 miles");
            ga.setText("0 gallons");
        }
    }

    private String calcMPG(Vector<NewDataPoint> v) {
        double mpg = 0, numV = v.size(), ga;

        for (Object d : v) {
            if (d instanceof NewDataPoint) {
                ga = ((NewDataPoint) d).getGA();
                mpg += ((NewDataPoint) d).getMSL() / ga;
            }
        }

        mpg /= numV;
        return L10NManager.getInstance().format(mpg, 3) + " mpg";
    }

    private String calcCPG(Vector<NewDataPoint> v) {
        double cpg = 0, numV = v.size();

        for (Object d : v)
            if (d instanceof NewDataPoint)
                cpg += ((NewDataPoint) d).getCTF() / ((NewDataPoint) d).getGA();

        cpg /= numV;
        return L10NManager.getInstance().formatCurrency(cpg);
    }

    private String calcCPM(Vector<NewDataPoint> v) {
        double cpm = 0, numV = v.size();

        for (Object d : v)
            if (d instanceof NewDataPoint)
                cpm += ((NewDataPoint) d).getCTF() / ((NewDataPoint) d).getMSL();

        cpm /= numV;
        return L10NManager.getInstance().formatCurrency(cpm);
    }

    private String calcDBF(Vector<NewDataPoint> v) {
        double dbf = 0, numV = v.size();

        for (Object d : v)
            if (d instanceof NewDataPoint)
                dbf += ((NewDataPoint) d).getMSL();

        dbf /= numV;
        return L10NManager.getInstance().format(dbf, 3) + " miles";
    }

    private String calcGA(Vector<NewDataPoint> v) {
        double ga = 0, numV = v.size();

        for (Object d : v)
            if (d instanceof NewDataPoint)
                ga += ((NewDataPoint) d).getGA();

        ga /= numV;
        return L10NManager.getInstance().format(ga, 3) + " gallons";
    }

    @SuppressWarnings("deprecation")
    private void atRunTime() {
        Car c;
        Double VERSION = 0.5;
        startDialog();

        if (Storage.getInstance().exists("dataPoints") || !Storage.getInstance().exists("name")) {
            root = showForm("addCar",null);
            root.setBackCommand(null);
            findCarCancel(root).setEnabled(false);

            Storage.getInstance().writeObject("version",VERSION);
        }  else if (Storage.getInstance().exists("name") && !Storage.getInstance().exists("version")) {
            curCar = (String)Storage.getInstance().readObject("name");
            c = (Car)Storage.getInstance().readObject(curCar);

            root = showForm("addCar",null);
            root.setBackCommand(null);
            findCarCancel(root).setEnabled(false);

            findCarName(root).setText(c.getName());
            findCurrentMilage(root).setText(Double.toString(c.getMileage()));
            if (c.getOdometer())
                findOdometerButton(root).setSelected(true);
            else
                findTripButton(root).setSelected(true);

            Storage.getInstance().writeObject("version",VERSION);
        } else if (Storage.getInstance().exists("name")) {
            curCar = (String)Storage.getInstance().readObject("name");
        }

        if (Storage.getInstance().exists("version") && !Storage.getInstance().readObject("version").equals(VERSION) &&
                curCar != null) {
            NewCar nc = (NewCar)Storage.getInstance().readObject(curCar);
            NewDCar nd = new NewDCar(nc);

            Storage.getInstance().writeObject(curCar, nd);
            Storage.getInstance().writeObject("version", VERSION);

            postTab(root);
        }
    }

    private void startDialog() {
        final Dialog d = new Dialog();
        Style s = new Style();
        final Button b = new Button("Okay");
        Container o = new Container(new BorderLayout());
        Container x = new Container(new FlowLayout(Component.CENTER));
        SpanLabel l = new SpanLabel("Thank you for being a part of my first app's alpha phase. " +
                "Please feel free to contact me with any concerns, issues, or suggestions.\n" +
                "Facebook page: facebook.com/groups/AlmadaDev/\n" +
                "Repo page: bitbucket.org/RAlmada/gas-tracker\n" +
                "Issue Tracker: bitbucket.org/RAlmada/gas-tracker/issues");
        Label t = new Label("Welcome!");

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                d.dispose();
            }
        });
        b.setWidth(d.getPreferredW() / 2);

        t.setUIID("DialogTitle");
        l.setTextUIID("Label2");

        s.setBgColor(0);
        s.setFgColor(255);

        d.setDialogStyle(s);
        d.setTitleComponent(t);
        d.setHeight(d.getPreferredH() * 2);
        d.setHeight(d.getHeight()*2);

        x.addComponent(b);

        o.addComponent(BorderLayout.CENTER, l);
        o.addComponent(BorderLayout.SOUTH, x);

        d.addComponent(o);

        final Command back = new Command("Back");
        b.getComponentForm().addCommandListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if (evt.getCommand() == back)
                    d.dispose();
            }
        });
        b.getComponentForm().setBackCommand(back);
        d.setDisposeWhenPointerOutOfBounds(true);
        d.show(Display.getInstance().getDisplayHeight()/6,
                Display.getInstance().getDisplayHeight()/6,
                0,0);
    }

    @Override
    protected void postTab(Form f) {
        if (root != null)
            if (root.getName().equals("Details"))
                findMenu(f).setSelectedIndex(2);

        curNdx = -1;

        root = f;
        if (!Storage.getInstance().exists("dataPoints") && curCar != null)
            updatePrevData();

//        Storage.getInstance().deleteStorageFile("Durango");
//        Storage.getInstance().deleteStorageFile("name");
//        Storage.getInstance().deleteStorageFile("version");
//        Storage.getInstance().deleteStorageFile("dataPoints");

//        System.out.println(curCar == null && Storage.getInstance().exists("name") && Storage.getInstance().exists("version"));

        if (curCar == null && Storage.getInstance().exists("name") && Storage.getInstance().exists("version"))
            curCar = (String)Storage.getInstance().readObject("name");

//        System.out.println(curCar == null);

        if (curCar != null && (Double)Storage.getInstance().readObject("version") > 0.4) {
            findCurrentCarLabel(root).setText("Currently Tracking: " + curCar);
            if (((NewDCar) Storage.getInstance().readObject(curCar)).getOdometer())
                findDecisionLabel(root).setText("Current odometer reading:");
            else
                findDecisionLabel(root).setText("Miles since last tank fill-up:");

            updatePrevData();
        }
    }

    @Override
    protected void postDetails(Form f) {
        root = f;

        if (curCar == null && Storage.getInstance().exists("name"))
            curCar = (String)Storage.getInstance().readObject("name");

        if (curNdx >= 0 && curCar != null) {
            NewDCar c = (NewDCar)Storage.getInstance().readObject(curCar);
            NewDataPoint d = c.getData().get(curNdx);
            Vector<NewDataPoint> v = new Vector<NewDataPoint>();
            v.add(d);

            findDetailsTitleLabel(root).setText((curNdx+1) + ") Fill-Up from " + d.getDate() + " for " + curCar);
            findMTF(root).setText(L10NManager.getInstance().format(d.getMileage(), 3) + " miles");
            findGAdd(root).setText(L10NManager.getInstance().format(d.getGA(), 3) + " gallons");
            findDBF(root).setText(L10NManager.getInstance().format(d.getMSL(), 3) + " miles");
            findCost(root).setText(L10NManager.getInstance().formatCurrency(d.getCTF()));
            findMPG(root).setText(calcMPG(v));
            findCPG(root).setText(calcCPG(v));
            findCPM(root).setText(calcCPM(v));
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    protected void onAddCar_CarSubmitAction(Component c, ActionEvent event) {
        NewDCar car = new NewDCar();

        if (findOdometerButton(root).isSelected())
            car.setOdometer(true);
        else
            car.setOdometer(false);

        try {
            if (!findCarName(root).getText().equals("")) {
                car.setName(findCarName(root).getText());
            } else {
                onSubmitThrow("Error", "Car name must be filled in.");
                return;
            }
            car.setMileage(Double.parseDouble(findCurrentMilage(root).getText()));
            car.setTankSize(Double.parseDouble(findGasTankSize(root).getText()));

            if (Storage.getInstance().exists("dataPoints")) {
                car.setData(newData((Vector<DataPoint>) Storage.getInstance().readObject("dataPoints"), car.getMileage()));

                Storage.getInstance().deleteStorageFile("dataPoints");
            } else if (curCar != null) {
                if (Storage.getInstance().exists(curCar)) {
                    car.setData(newData(((Car) Storage.getInstance().readObject(curCar)).getData(), car.getMileage()));
                }
            } else if (Storage.getInstance().exists("name")) {
                curCar = (String) Storage.getInstance().readObject("name");
                if (Storage.getInstance().exists(curCar))
                    car.setData(newData(((Car) Storage.getInstance().readObject(curCar)).getData(), car.getMileage()));
            } else {
                car.setData(new Vector<NewDataPoint>());
            }
        } catch (NumberFormatException e) {
            onSubmitThrow("Error", "Current Odometer Reading and Size of Gas Tank need to be filled in.");
        }

        curCar = car.getName();
        Storage.getInstance().writeObject("name", curCar);
        Storage.getInstance().writeObject(curCar, car);

        findCurrentCarLabel(mainRoot).setText("Currently Tracking: " + curCar);
        if (car.getOdometer())
            findDecisionLabel(mainRoot).setText("Current odometer reading:");
        else
            findDecisionLabel(mainRoot).setText("Miles since last tank fill-up:");

        mainRoot.show();
        root = mainRoot;

        findCurrentOdo(root).setText(L10NManager.getInstance().format(car.getMileage(), 2) + " miles");
        updatePrevData();
    }

    @SuppressWarnings("deprecation")
    private static Vector<NewDataPoint> newData(Vector<DataPoint> old, double mileage) {
        Vector<NewDataPoint> nd = new Vector<NewDataPoint>(), tmp = new Vector<NewDataPoint>();

        tmp.add(new NewDataPoint(old.get(old.size()-1), mileage * -1));

        for (int i = old.size()-2, j = 0; i >= 0; i--, j++)
            tmp.add(new NewDataPoint(old.get(i), tmp.get(j).getMileage() - tmp.get(j).getMSL()));

        for (int j = tmp.size()-1; j >= 0; j--)
            nd.add(tmp.get(j));

        return nd;
    }

    @Override
    protected void onDetails_EditButtonAction(Component c, ActionEvent event) {
        NewDCar car = (NewDCar)Storage.getInstance().readObject(curCar);
        curNdx = Character.digit((findDetailsTitleLabel(root).getText().charAt(0)),10) - 1;
        NewDataPoint d = car.getData().get(curNdx);

        root = showForm("editData",null);

        if (car.getOdometer()) {
            findDecisionLabel(root).setText("Odometer reading at time of fill:");
            findMilesSinceFillup(root).setText(Double.toString(d.getMileage()));
        } else {
            findDecisionLabel(root).setText("Miles since previous tank fill-up:");
            findMilesSinceFillup(root).setText(Double.toString(d.getMSL()));
        }

        findDateField(root).setText(d.getDate());
        findGallonsAdded(root).setText(L10NManager.getInstance().format(d.getGA(),3));
        findCostOfFillup(root).setText(L10NManager.getInstance().format(d.getCTF(),2));

        if (curNdx == 0)
            findCompletefillno(root).setEnabled(false);
    }

    @Override
    protected void onDetails_DeleteButtonAction(Component c, ActionEvent event) {
        int ndx = Character.digit((findDetailsTitleLabel(root).getText().charAt(0)),10) - 1;

        confirmDeleteDialog("Confirm", "Are you sure you would like to delete this data point?", ndx);
    }

    @Override
    protected void onEditData_DoneAction(Component c, ActionEvent event) {
        if (curCar == null && Storage.getInstance().exists("name"))
            curCar = (String)Storage.getInstance().readObject("name");

        if (curNdx >= 0 && curCar != null) {
            try {
                NewDCar car = (NewDCar) Storage.getInstance().readObject(curCar);
                Vector<NewDataPoint> v = car.getData();
                NewDataPoint d = v.get(curNdx);
                NewDataPoint newD = new NewDataPoint();

                newD.setDate(findDateField(root).getText());
                newD.setGA(Double.parseDouble(findGallonsAdded(root).getText()));
//                if (newD.getGA() > car.getTankSize()){
//                    onSubmitThrow("Error", "Cannot add more gas than size of tank: " + car.getTankSize());
//                    return;
//                }
                newD.setCTF(L10NManager.getInstance().parseCurrency(findCostOfFillup(root).getText()));
                if (car.getOdometer())
                    if (curNdx != 0)
                        newD.setMSL(L10NManager.getInstance().parseDouble(findMilesSinceFillup(root).getText()) -
                                v.get(curNdx - 1).getMileage());
                    else
                        newD.setMSL((L10NManager.getInstance().parseDouble(findMilesSinceFillup(root).getText()) -
                                v.get(curNdx).getMileage()) + v.get(curNdx).getMSL());
                else
                    newD.setMSL(L10NManager.getInstance().parseDouble(findMilesSinceFillup(root).getText()));

                if (curNdx == v.size()-1)
                    newD.setMileage((d.getMileage() - d.getMSL()) + newD.getMSL());

                if (!d.equals(newD)) {
                    if (findCompletefillyes(root).isSelected()) {
                        v.setElementAt(newD, curNdx);
                    } else {
                        newD.setGA(newD.getGA() + v.get(curNdx - 1).getGA());
                        newD.setMSL(newD.getMSL() + v.get(curNdx - 1).getMSL());
                        newD.setCTF(newD.getCTF() + v.get(curNdx - 1).getCTF());
                        v.setElementAt(newD, curNdx - 1);
                        v.remove(curNdx);
                        curNdx--;
                    }
                    car.setData(v);
                    updateMileage(car);
                    Storage.getInstance().writeObject(curCar, car);
                    onEditData_CancelAction(c,event);
                }
            } catch (NumberFormatException e) {
                onSubmitThrow("Error", "Gallons added, miles since fill/odometer reading, and cost must be numbers!");
            } catch (ArithmeticException e) {
                onSubmitThrow("Error", "Gallons added, miles since fill/odometer reading, and cost must be positive!");
            } catch (IllegalArgumentException e) {
                onSubmitThrow("Error", "Date must be filled in! (MM/DD/YY)");
            } catch (IndexOutOfBoundsException e) {
                onSubmitThrow("Error", "Illegal date! (MM/DD/YY)");
            }
        }
    }

    @Override
    protected void onEditData_CancelAction(Component c, ActionEvent event) {
        root = showForm("Details",null);
        root.setBackCommand(new Command("back") {
            public void actionPerformed(ActionEvent e) {
                mainRoot = showForm("Tab",null);
            }
        });
    }
}
