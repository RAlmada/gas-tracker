package com.almadadev.gastracker;

import com.codename1.io.Externalizable;
import com.codename1.io.Util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Vector;

/**
 * Created by Robert J. Almada on 6/26/14.
 * Edited by Robert J. Almada on 8/12/14.
 * Replaced Car on 6/26/14
 * Replaced by NewDCar on 8/12/14
 */
@Deprecated
public class NewCar implements Externalizable {
    private String name;
    private double mileage;
    private boolean odometer;
    private double tankSize;
    private Vector<DataPoint> data;

    public NewCar() {

    }

    public NewCar(String name, double mileage, boolean odometer, double tankSize, Vector<DataPoint> data) {
        this.name = name;
        this.mileage = mileage;
        this.odometer = odometer;
        this.tankSize = tankSize;
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    public boolean getOdometer() {
        return odometer;
    }

    public void setOdometer(boolean odometer) {
        this.odometer = odometer;
    }

    public double getTankSize() {
        return tankSize;
    }

    public void setTankSize(double tankSize) {
        this.tankSize = tankSize;
    }

    public Vector<DataPoint> getData() {
        return data;
    }

    public void setData(Vector<DataPoint> data) {
        this.data = data;
    }

    @Override
    public int getVersion() {
        return 1;
    }

    @Override
    public void externalize(DataOutputStream out) throws IOException {
        Util.writeUTF(name, out);
        out.writeDouble(mileage);
        out.writeBoolean(odometer);
        out.writeDouble(tankSize);
        Util.writeObject(data, out);
    }

    @Override
    public void internalize(int version, DataInputStream in) throws IOException {
        name = Util.readUTF(in);
        mileage = in.readDouble();
        odometer = in.readBoolean();
        tankSize = in.readDouble();
        data = (Vector<DataPoint>)Util.readObject(in);
    }

    @Override
    public String getObjectId() {
        return "NewCar";
    }
}
