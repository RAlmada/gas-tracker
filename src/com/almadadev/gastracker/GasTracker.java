package com.almadadev.gastracker;

import com.codename1.ui.Display;
import com.codename1.ui.Form;
import userclasses.StateMachine;

/**
 * Created by Robert J. Almada on 5/31/14.
 * Edited by Robert J. Almada on 7/11/14.
 */
public class GasTracker {
    private Form current;

    public void init(Object context) {
    }

    public void start() {
        if(current != null){
            current.show();
            return;
        }
        new StateMachine("/theme");
    }

    public void stop() {
        current = Display.getInstance().getCurrent();
    }
    
    public void destroy() {
    }
}
