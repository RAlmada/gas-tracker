package com.almadadev.gastracker;

import com.codename1.io.Externalizable;
import com.codename1.io.Util;
import me.regexp.RE;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by Robert J. Almada on 8/12/14.
 * Edited by Robert J. Almada on 8/12/14.
 * Replaced DataPoint on 8/12/14
 */
@SuppressWarnings("unused")
public class NewDataPoint implements Externalizable {
    private String date;
    private double GA;
    private double MSL;
    private double CTF;
    private double mileage;

    public NewDataPoint() {

    }

    public NewDataPoint(DataPoint d, double mileage) {
        this.date = d.getDate();
        this.GA = d.getGA();
        this.MSL = d.getMSL();
        this.CTF = d.getCTF();
        this.mileage = mileage;

//        if (mileage < 0) {
//            this.mileage = mileage * -1;
//        } else {
//            this.mileage = mileage - this.MSL;
//        }
    }

    public NewDataPoint(String date, double GA, double MSL, double CTF, double mileage) throws IndexOutOfBoundsException, IllegalArgumentException {
        RE r = new RE("(0[0-9]|1[0-2])/([0-2][0-9]|3[0-1])/[0-9]{2}");
        if (date == null || date.equals(""))
            throw new IllegalArgumentException();
        if (r.match(date))
            this.date = date;
        else
            throw new IndexOutOfBoundsException();
        this.GA = GA;
        this.MSL = MSL;
        this.CTF = CTF;
        this.mileage = mileage;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) throws IndexOutOfBoundsException, IllegalArgumentException {
        RE r = new RE("(0[0-9]|1[0-2])/([0-2][0-9]|3[0-1])/[0-9]{2}");
        if (date == null || date.equals(""))
            throw new IllegalArgumentException();
        if (r.match(date))
            this.date = date;
        else
            throw new IndexOutOfBoundsException();
    }

    public double getCTF() {
        return CTF;
    }

    public void setCTF(double CTF) {
        this.CTF = CTF;
    }

    public double getGA() {
        return GA;
    }

    public void setGA(double GA) {
        this.GA = GA;
    }

    public double getMSL() {
        return MSL;
    }

    public void setMSL(double MSL) {
        this.MSL = MSL;
    }

    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    @Override
    public int getVersion() {
        return 1;
    }

    @Override
    public void externalize(DataOutputStream out) throws IOException {
        Util.writeUTF(date, out);
        out.writeDouble(GA);
        out.writeDouble(MSL);
        out.writeDouble(CTF);
        out.writeDouble(mileage);
    }

    @Override
    public void internalize(int version, DataInputStream in) throws IOException {
        date = Util.readUTF(in);
        GA = in.readDouble();
        MSL = in.readDouble();
        CTF = in.readDouble();
        mileage = in.readDouble();
    }

    @Override
    public String getObjectId() {
        return "NewDataPoint";
    }
}
