package com.almadadev.gastracker;

import com.codename1.io.Externalizable;
import com.codename1.io.Util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Vector;

/**
 * Created by Robert J. Almada on 8/12/14.
 * Edited by Robert J. Almada on 8/12/14.
 * Replaced NewCar on 8/12/14
 */
public class NewDCar implements Externalizable {
    private String name;
    private double mileage;
    private boolean odometer;
    private double tankSize;
    private Vector<NewDataPoint> data;

    public NewDCar() {

    }

    public NewDCar(NewCar c) {
        this.name = c.getName();
        this.mileage = c.getMileage();
        this.odometer = c.getOdometer();
        this.tankSize = c.getTankSize();
        this.data = new Vector<NewDataPoint>();

        if (c.getData().size() > 0) {
            DataPoint last = c.getData().lastElement();
            Vector<NewDataPoint> tmp = new Vector<NewDataPoint>();

            tmp.add(new NewDataPoint(last, this.mileage));

            for (int i = c.getData().size()-2, j = 0; i >= 0; i--, j++)
                tmp.add(new NewDataPoint(c.getData().get(i), tmp.get(j).getMileage() - tmp.get(j).getMSL()));

            for (int j = tmp.size()-1; j >= 0; j--)
                this.data.add(tmp.get(j));
        }
    }

    public NewDCar(String name, double mileage, boolean odometer, double tankSize, Vector<NewDataPoint> data) {
        this.name = name;
        this.mileage = mileage;
        this.odometer = odometer;
        this.tankSize = tankSize;
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    public boolean getOdometer() {
        return odometer;
    }

    public void setOdometer(boolean odometer) {
        this.odometer = odometer;
    }

    public double getTankSize() {
        return tankSize;
    }

    public void setTankSize(double tankSize) {
        this.tankSize = tankSize;
    }

    public Vector<NewDataPoint> getData() {
        return data;
    }

    public void setData(Vector<NewDataPoint> data) {
        this.data = data;
    }

    @Override
    public int getVersion() {
        return 1;
    }

    @Override
    public void externalize(DataOutputStream out) throws IOException {
        Util.writeUTF(name, out);
        out.writeDouble(mileage);
        out.writeBoolean(odometer);
        out.writeDouble(tankSize);
        Util.writeObject(data, out);
    }

    @Override
    public void internalize(int version, DataInputStream in) throws IOException {
        name = Util.readUTF(in);
        mileage = in.readDouble();
        odometer = in.readBoolean();
        tankSize = in.readDouble();
        data = (Vector<NewDataPoint>)Util.readObject(in);
    }

    @Override
    public String getObjectId() {
        return "NewDCar";
    }
}
